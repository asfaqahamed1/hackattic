module hackattic

go 1.21.0

require (
	github.com/makiuchi-d/gozxing v0.1.1 // indirect
	golang.org/x/crypto v0.17.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
