package main

import (
	"fmt"
	"hackattic/solutions"
)

func main() {
	fmt.Println(`
	This is project of solutions for the Hackattic problems 
	https://hackattic.com/challenges/help_me_unpack
	`)
	solutions.PasswordUnhash()
}
