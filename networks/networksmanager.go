package networks

import (
	"bytes"
	"encoding/json"
	"net/http"
)

func GetRequest(url string) (*http.Response, error) {
	res, err := http.Get(url)
	return res, err
}

func PostRequest(url string, payload any) (*http.Response, error) {
	jsonBody, err := json.Marshal(payload)
	if err != nil {
		return nil, err
	}
	response, err := http.Post(url, "application/json", bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}
	return response, err
}
