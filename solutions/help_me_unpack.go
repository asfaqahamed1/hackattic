package solutions

import (
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"hackattic/networks"
	"io"
	"log"
	"math"
)

type HelpMeUnpackType struct {
	Bytes string `json:"bytes"`
}
type ResponseJson struct {
	Int             int32   `json:"int"`
	Unit            uint32  `json:"uint"`
	Short           int16   `json:"short"`
	Float           float32 `json:"float"`
	Double          float64 `json:"double"`
	BigDoubleEndian float64 `json:"big_endian_double"`
}

func HelpMeUnpack() {
	base64String, err := getBase64String()
	if err != nil {
		log.Fatal(err)
	}
	bytesArray, err := base64.StdEncoding.DecodeString(base64String)
	unpackedJson := getUnpackedJsonFromBytesArray(bytesArray)
	response, err := networks.PostRequest("https://hackattic.com/challenges/help_me_unpack/solve?access_token=a92ab06dcf26d3d7", unpackedJson)
	defer response.Body.Close()
	responseByte, err := io.ReadAll(response.Body)
	println(string(responseByte))
}

func getUnpackedJsonFromBytesArray(bytesArray []byte) ResponseJson {
	unpackedJson := ResponseJson{
		// byte 1 to 4
		Int: int32(binary.LittleEndian.Uint32(bytesArray[0:4])),
		// byte 5 to 8
		Unit: binary.LittleEndian.Uint32(bytesArray[4:8]),
		// bytes 9 & 10
		Short: int16(binary.LittleEndian.Uint16(bytesArray[8:10])),
		// bytes 13 to 16
		Float: math.Float32frombits(binary.LittleEndian.Uint32(bytesArray[12:16])),
		// bytes 16 to 24
		Double: math.Float64frombits(binary.LittleEndian.Uint64(bytesArray[16:24])),
		// bytes 25 to 32
		BigDoubleEndian: math.Float64frombits(binary.BigEndian.Uint64(bytesArray[24:32])),
	}
	return unpackedJson

}

func getBase64String() (string, error) {
	response, err := networks.GetRequest("https://hackattic.com/challenges/help_me_unpack/problem?access_token=a92ab06dcf26d3d7")
	if err != nil {
		return "", err
	}
	defer response.Body.Close()
	var byteResponse *HelpMeUnpackType
	responseObject, err := io.ReadAll(response.Body)
	if err != nil {
		return "", err
	}
	err = json.Unmarshal(responseObject, &byteResponse)
	if err != nil {
		return "", err
	}
	return byteResponse.Bytes, nil
}
