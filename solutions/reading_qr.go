package solutions

import (
	"encoding/json"
	"hackattic/networks"
	"image"
	_ "image/png"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/makiuchi-d/gozxing"
	"github.com/makiuchi-d/gozxing/qrcode"
)

type ImageResponse struct {
	Url string `json:"image_url"`
}

type ApiResponse struct {
	Code string `json:"code"`
}

func Readingqr() {
	imageUrl, err := getImageUrl()
	if err != nil {
		log.Fatal(err)
	}
	log.Print(imageUrl)
	img := getMessedUpImage(imageUrl)
	imageCode := decodeQr(img)
	response := imageCode.String()
	var responsePayload = ApiResponse{
		Code: response,
	}
	res, err := networks.PostRequest("https://hackattic.com/challenges/reading_qr/solve?access_token=a92ab06dcf26d3d7", &responsePayload)
	if err != nil {
		log.Fatal(err.Error())
	}
	defer res.Body.Close()
	resp, err := io.ReadAll(res.Body)
	print(string(resp))
}

func getImageUrl() (string, error) {
	response, err := networks.GetRequest("https://hackattic.com/challenges/reading_qr/problem?access_token=a92ab06dcf26d3d7")

	if err != nil {
		return "", err
	}
	defer response.Body.Close()
	responseBody, err := io.ReadAll(response.Body)
	if err != nil {
		return "", err
	}
	var imageResponse *ImageResponse
	if err := json.Unmarshal(responseBody, &imageResponse); err != nil {
		return "", err
	}
	return imageResponse.Url, nil
}

func decodeQr(img image.Image) *gozxing.Result {
	binaryMap, _ := gozxing.NewBinaryBitmapFromImage(img)
	qrReader := qrcode.NewQRCodeReader()
	resultImg, _ := qrReader.Decode(binaryMap, nil)
	return resultImg
}

func getMessedUpImage(url string) image.Image {

	response, err := networks.GetRequest(url)
	if err != nil {
		return nil
	}
	defer response.Body.Close()
	fileName := "qrImage"
	storeImageInFile(fileName, response)
	file, _ := os.Open(fileName)
	img, _, err := image.Decode(file)
	if err != nil {
		log.Fatalf(err.Error())
		return nil
	}
	return img
}

func storeImageInFile(filename string, img *http.Response) {
	file, err := os.Create(filename)
	if err != nil {
		return
	}
	defer file.Close()
	_, err = io.Copy(file, img.Body)
	return
}
