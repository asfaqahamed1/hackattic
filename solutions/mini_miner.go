package solutions

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"hackattic/networks"
	"io"
	"log"
	"math"
)

func isAllChar0(str string) bool {
	for _, char := range str {
		if char != '0' {
			return false
		}
	}
	return true
}

type MineBlock struct {
	Data  interface{} `json:"data"`
	Nonce int         `json:"nonce"`
}

type MineData struct {
	Difficulty int       `json:"difficulty"`
	Block      MineBlock `json:"block"`
}

func Miniminer() {
	response, err := networks.GetRequest("https://hackattic.com/challenges/mini_miner/problem?access_token=a92ab06dcf26d3d7")
	if err != nil {
		log.Fatal(err.Error())
	}
	resByte, err := io.ReadAll(response.Body)
	println(string(resByte))
	var miningData MineData
	if err = json.Unmarshal(resByte, &miningData); err != nil {
		log.Fatal(err.Error())
	}
	staisfiedNonce := hashAndMine(miningData.Difficulty, miningData.Block)
	responsePayload := map[string]int{
		"nonce": staisfiedNonce,
	}
	res, err := networks.PostRequest("https://hackattic.com/challenges/mini_miner/solve?access_token=a92ab06dcf26d3d7", responsePayload)
	defer res.Body.Close()
	responseByte, err := io.ReadAll(res.Body)
	println(string(responseByte))
}

func hashAndMine(target int, mineData MineBlock) int {
	bytesRequired := (target + 7) / 8
	mineUpto := math.Pow(2, float64(target))

	for i := 0; i <= int(mineUpto); i++ {
		mineData.Nonce = i
		mineString, err := json.Marshal(mineData)
		if err != nil {
			log.Fatalf(err.Error())
		}
		sha256 := getSha256String(string(mineString))
		isStatisfied := isSatisfiedNonce(sha256, target, bytesRequired)
		if isStatisfied {
			return i
		}
	}
	return 1
}

func getSha256String(mineString string) string {
	hashCreator := sha256.New()
	hashCreator.Write([]byte(mineString))
	sha256 := hex.EncodeToString(hashCreator.Sum(nil))
	return sha256
}
func isSatisfiedNonce(sha256 string, target int, bytes int) bool {
	hashString, _ := hex.DecodeString(sha256)
	hashString = hashString[:bytes]
	var stringLiteral string
	for _, byt := range hashString {
		stringLiteral += fmt.Sprintf("%08b", byt)
	}
	if len(stringLiteral) < target {
		return false
	}
	isSatisfied := isAllChar0(stringLiteral[:target])
	return isSatisfied
}
