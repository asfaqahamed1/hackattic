package solutions

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"hackattic/networks"
	"io"
	"log"

	"golang.org/x/crypto/pbkdf2"
	"golang.org/x/crypto/scrypt"
)

type RequestData struct {
	Password string `json:"password"`
	Salt     string `json:"salt"`
	Pbkdf2   Pbkdf2 `json:"pbkdf2"`
	Scrypt   Scrypt `json:"scrypt"`
}

type Pbkdf2 struct {
	Rounds int    `json:"rounds"`
	Hash   string `json:"hash"`
}

type Scrypt struct {
	N       int
	R       int    `json:"r"`
	P       int    `json:"p"`
	Buflen  int    `json:"buflen"`
	Control string `json:"_control"`
}

type ResponseBody struct {
	Sha256 string `json:"sha256"`
	Hmac   string `json:"hmac"`
	Pbdkf2 string `json:"pbkdf2"`
	Scrypt string `json:"scrypt"`
}

func PasswordUnhash() {
	response, err := networks.GetRequest("https://hackattic.com/challenges/password_hashing/problem?access_token=a92ab06dcf26d3d7")
	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()
	byteData, err := io.ReadAll(response.Body)
	var requestData RequestData
	if err = json.Unmarshal(byteData, &requestData); err != nil {
		log.Fatal(err)
	}
	byteKey, err := base64.StdEncoding.DecodeString(requestData.Salt)
	if err != nil {
		log.Fatal(err)
	}
	sha256 := sha256.Sum256([]byte(requestData.Password))
	hmacsha256 := getHmac([]byte(requestData.Password), byteKey)
	pbkdf2Hash := getPbkdf2([]byte(requestData.Password), byteKey, requestData.Pbkdf2.Rounds)
	scrypt, _ := getScrypt([]byte(requestData.Password), byteKey, requestData.Scrypt.R, requestData.Scrypt.N, requestData.Scrypt.P)

	responseBody := ResponseBody{
		Sha256: hex.EncodeToString(sha256[:]),
		Hmac:   hex.EncodeToString(hmacsha256),
		Pbdkf2: hex.EncodeToString(pbkdf2Hash),
		Scrypt: hex.EncodeToString(scrypt),
	}
	sumbitResponse, err := networks.PostRequest("https://hackattic.com/challenges/password_hashing/solve?access_token=a92ab06dcf26d3d7", responseBody)
	if err != nil {
		log.Fatal(err)
	}
	defer sumbitResponse.Body.Close()
	responseByte, err := io.ReadAll(sumbitResponse.Body)
	fmt.Println(string(responseByte))

}

func getHmac(password, salt []byte) []byte {
	hmac := hmac.New(sha256.New, salt)
	hmac.Write(password)
	passwordMac := hmac.Sum(nil)
	return passwordMac
}

func getPbkdf2(password, salt []byte, iter int) []byte {
	return pbkdf2.Key(password, salt, iter, 32, sha256.New)
}

func getScrypt(password, salt []byte, r, n, p int) ([]byte, error) {
	if scryptHash, err := scrypt.Key(password, salt, n, r, p, 32); err == nil {
		return scryptHash, nil
	} else {
		return nil, err
	}
}
